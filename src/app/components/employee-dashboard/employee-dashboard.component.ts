import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/shared/api.service';
import { EmployeeModel } from './employee.model';

@Component({
  selector: 'app-employee-dashboard',
  templateUrl: './employee-dashboard.component.html',
  styleUrls: ['./employee-dashboard.component.css']
})
export class EmployeeDashboardComponent implements OnInit {
  formValue!: FormGroup;
  employeeModelObject: EmployeeModel = new EmployeeModel();
  employeeData!: any;
  showUpdate!: boolean;
  showAdd!: boolean;

  constructor(private formBuilder: FormBuilder, private api: ApiService) { }

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: [''],
      mobile: [''],
      salary: [''],
    })

    this.getAllEmployee();
  }

  clickAddEmploy() {
    this.formValue.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }

  getAllEmployee() {
    this.api.getEmployee().subscribe(res => {
      this.employeeData = res;
    })
  }

  postEmployee() {
    this.employeeModelObject.firstName = this.formValue.value.firstName;
    this.employeeModelObject.lastName = this.formValue.value.lastName;
    this.employeeModelObject.email = this.formValue.value.email;
    this.employeeModelObject.mobile = this.formValue.value.mobile;
    this.employeeModelObject.salary = this.formValue.value.salary;

    this.api.postEmployee(this.employeeModelObject)
      .subscribe(res => {
        console.log(res);
        alert("Employee add successfully");

        let ref = document.getElementById('cancel');
        ref?.click();

        this.formValue.reset();
        this.getAllEmployee();
      }, err => {
        alert("Something went wrong");
      })
  }

  deleteEmployee(employee: any) {
    this.api.deleteEmployee(employee.id).subscribe(res => {
      alert("Employee deleted");

      this.getAllEmployee();
    })
  }

  onEdit(employee: any) {
    this.showAdd = false;
    this.showUpdate = true;
    this.employeeModelObject.id = employee.id;
    this.formValue.get('firstName')?.setValue(employee.firstName);
    this.formValue.get('lastName')?.setValue(employee.lastName);
    this.formValue.get('email')?.setValue(employee.email);
    this.formValue.get('mobile')?.setValue(employee.mobile);
    this.formValue.get('salary')?.setValue(employee.salary);
  }

  updatedEmployee() {
    this.employeeModelObject.firstName = this.formValue.value.firstName;
    this.employeeModelObject.lastName = this.formValue.value.lastName;
    this.employeeModelObject.email = this.formValue.value.email;
    this.employeeModelObject.mobile = this.formValue.value.mobile;
    this.employeeModelObject.salary = this.formValue.value.salary;

    this.api.updatedEmployee(this.employeeModelObject, this.employeeModelObject.id)
      .subscribe(res => {
        alert("Updated succesfully")

        let ref = document.getElementById('cancel');
        ref?.click();

        this.formValue.reset();
        this.getAllEmployee();
      })
  }
}
